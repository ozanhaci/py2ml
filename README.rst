py2ml Python Markup Library
===========================

.. image:: //api.codacy.com/project/badge/grade/66b183e399334449896b62189a5f8e32
   :target: https://www.codacy.com/app/ozan_haci/py2ml

.. image:: //ci.appveyor.com/api/projects/status/jvusft0kr032ds1n?svg=true

.. image:: //img.shields.io/badge/Python-2.7-blue.svg

.. image:: //img.shields.io/:license-MIT-blue.svg
   :alt: MIT License

py2ml library is to generate HTML, XML outputs from python objects.

py2ml can be used as a supplement for template engines or can be used

instead of template engines for straightforward cases.

Currently only Python 2.7 is supported.

Sample codes and usage: http://wiki.ozanh.com/doku.php?id=python:py2ml:examples

    >>> import py2ml
    >>> root = py2ml.Root()
    >>> html = py2ml.Node(root, tag="html")
    >>> body = html.append("body")
    >>> print root # doctest: +NORMALIZE_WHITESPACE
    <html>
    <body>
    </body>
    </html>
    
    >>> html['lang'] = 'en'
    >>> print root
    <html lang="en">
    <body>
    </body>
    </html>
    
    >>> body.attr(id="idBody") # doctest: +ELLIPSIS
    <Node ...>
    >>> print root
    <html lang="en">
    <body id="idBody">
    </body>
    </html>
    
    >>> form = py2ml.Node(tag="form",
    ...                   attr={"action": "/somepage.py", "method": "post"})
    >>> body.append(form) # doctest: +ELLIPSIS
    <Node ...>
    >>> print root
    <html lang="en">
    <body id="idBody">
    <form action="/somepage.py" method="post">
    </form>
    </body>
    </html>
    
    >>> print body
    <body id="idBody">
    <form action="/somepage.py" method="post">
    </form>
    </body>
    
    >>> form.after(py2ml.Node(content="Content After Form")) # doctest: +ELLIPSIS
    <Node ...>
    >>> print root
    <html lang="en">
    <body id="idBody">
    <form action="/somepage.py" method="post">
    </form>
    Content After Form
    </body>
    </html>
    
    >>> form.before(py2ml.Node(content="Content Before Form")) # doctest: +ELLIPSIS
    <Node ...>
    >>> print root
    <html lang="en">
    <body id="idBody">
    Content Before Form
    <form action="/somepage.py" method="post">
    </form>
    Content After Form
    </body>
    </html>