#!/usr/bin/python
# encoding: utf-8
# Copyright (c) 2015-2016 Ozan HACIBEKIROGLU
import sys
if not sys.version_info[:2] == (2, 7):
    sys.exit("Sorry, Only Python 2.7 is supported")
from setuptools import setup, find_packages

import py2ml


description = ''' py2ml is a simple library to build html and xml output with
python objects'''
long_description = open("README.rst").read()

setup(
    name="py2ml",
    version=py2ml.__version__,
    author="Ozan HACIBEKIROGLU",
    author_email="ozan_haci@yahoo.com",
    url="https://bitbucket.org/ozanhaci/py2ml",
    license="MIT",
    keywords="html xml xhtml markup template",
    test_suite="py2ml.tests",
    packages=find_packages(),
    description=description,
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Code Generators',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Operating System :: OS Independent',
    ],
)
