#!/usr/bin/python
# encoding: utf-8
from __future__ import print_function
import py2ml

root = py2ml.Root()
html = py2ml.Node(root, tag="html")
body = html.append("body")
print("Example1\n")
print(root)
#  output is
'''
<html>
<body>
</body>
</html>
'''

html["lang"] = "en"
print("\nExample2\n")
print(root)
#  output is
'''
<html lang="en">
<body>
</body>
</html>
'''

body.attr(id="idBody")
print("\nExample3\n")
print(root)
#  output is
'''
<html lang="en">
<body id="idBody">
</body>
</html>
'''

form = py2ml.Node(tag="form", attr={"action": "/somepage.py",
                                  "method": "post"})
body.append(form)
print("\nExample4\n")
print(root)
#  output is
'''
<html lang="en">
<body id="idBody">
<form action="/somepage.py" method="post">
</form>
</body>
</html>
'''

print("\nExample5\n")
print(body)
#  output is
'''
<body id="idBody">
<form action="/somepage.py" method="post">
</form>
</body>
'''

form.after(py2ml.Node(content="Content After Form"))
print("\nExample6\n")
print(root)
#  output is
'''
<html lang="en">
<body id="idBody">
<form action="/somepage.py" method="post">
</form>
Content After Form
</body>
</html>
'''

form.before(py2ml.Node(content="Content Before Form"))
print("\nExample7\n")
print(root)
#  output is
'''
<html lang="en">
<body id="idBody">
Content Before Form
<form action="/somepage.py" method="post">
</form>
Content After Form
</body>
</html>
'''

root2 = py2ml.Root(beautify=True)
html2 = py2ml.Node(root2, tag="html")
body2 = py2ml.Node(tag="body")
html2.append(body2)
p = body2.append(py2ml.Node(content="<p>This is a paragraph</p>"))
body2.prepend(py2ml.Node(content="<h1>This is page heading</h1>"))
div = p.after(py2ml.Node(tag="div"))
div.append(py2ml.Node(content="<span>This is div content</span>"))
print("\nExample8\n")
print(root2)
#  output is
'''
<html>
 <body>
  <h1>This is page heading</h1>
  <p>This is a paragraph</p>
  <div>
   <span>This is div content</span>
  </div>
 </body>
</html>
'''

