#!/usr/bin/python
# encoding: utf-8
from __future__ import print_function, unicode_literals
import weakref
from collections import OrderedDict

USE_WEAKREF = True
str = unicode

_html_escape_table = [("&", "&amp;"),
                     ('"', "&#34;"),
                     ("'", "&#39;"),
                     (">", "&gt;"),
                     ("<", "&lt;"),
                     ]

def escape(data):
    for k, v in _html_escape_table:
        data = data.replace(k, v)
    return data


def default_indentfunc(depth):
    return " "*depth


class Binding(object):

    def __setattr__(self, attr, val):
        if USE_WEAKREF and not isinstance(val, weakref.ProxyTypes):
            super(Binding, self).__setattr__(attr, weakref.proxy(val))
        else:
            super(Binding, self).__setattr__(attr, val)


class Methods(object):

    def _append_prepend(self, tag, attr, func, node):
        if isinstance(tag, basestring):
            tag = Node(parent=self, tag=tag, attr=attr)
        elif isinstance(tag, Node):
            if USE_WEAKREF:
                tag.parent = weakref.proxy(self)
            else:
                tag.parent = self
            tag.inherit_props()
            if attr is not None:
                tag.attr(attr)
        else:
            raise Exception()

        if func not in ("append", "prepend"):
            raise Exception("Wrong method")


        if node is not None and isinstance(node, Node):
            new_children = []
            for c in self.children:
                if c is node:
                    if func == "append":
                        new_children.append(c)
                        new_children.append(tag)
                    elif func == "prepend":
                        new_children.append(tag)
                        new_children.append(c)
                else:
                    new_children.append(c)
            self.children = new_children
        else:
            if func == "append":
                self.children.append(tag)
            elif func == "prepend":
                self.children.insert(0, tag)

        return tag

    def append(self, tag, attr=None, after=None):
        func = "append"
        return self._append_prepend(tag, attr, func, node=after)

    def prepend(self, tag, attr=None, before=None):
        func = "prepend"
        return self._append_prepend(tag, attr, func, node=before)


class Root(Methods):

    def __init__(self, beautify=False, indentfunc=default_indentfunc,
                 auto_escape=True):
        self._beautify = beautify
        self._indentfunc = indentfunc
        self._auto_escape = auto_escape
        self._depth = -1
        self.bind = Binding()
        self.children = []

    def __str__(self):
        return self.__unicode__().encode("utf-8")

    def __unicode__(self):
        output = []
        for i in self.children:
            output.append(i.__unicode__())
        return "".join(output)



class Node(Methods):

    self_closing_tags = ("input", "br", "hr", "img", "meta")

    def __init__(self, parent=None, tag="", content="", attr=None,
                 auto_escape=True):

        self.children = []
        self.attrs = OrderedDict()
        self._depth = 0
        self._auto_escape = auto_escape
        self.is_root = False
        self.content = content
        if parent:
            if USE_WEAKREF:
                self.parent = weakref.proxy(parent)
            else:
                self.parent = parent
        else:
            self.parent = parent
        self.tag = tag

        if attr is not None:
            self.attr(attr)
        if isinstance(parent, Root):
            self.parent.children.append(self)

        self.inherit_props()

    def after(self, tag, attr=None):
        if isinstance(tag, Node):
            self.parent.append(tag, attr=attr, after=self)
            return tag
        raise Exception()

    def before(self, tag, attr=None):
        if isinstance(tag, Node):
            self.parent.prepend(tag, attr=attr, before=self)
            return tag
        raise Exception()

    def attr(self, *args, **kwargs):
        for ar in args:
            if isinstance(ar, dict):
                self.attrs.update(ar)
        self.attrs.update(kwargs)
        return self

    def disown_child(self, child):
        while True:
            try:
                self.children.remove(child)
                continue
            except ValueError:
                break

    def replace_child(self, oldchild, newchild):
        for i in xrange(0, len(self.children)):
            if self.children[i] is oldchild:
                self.children[i] = newchild

    def inherit_props(self):
        if isinstance(self.parent, (Node, Root)):
            self._beautify = self.parent._beautify
            self._indentfunc = self.parent._indentfunc
            self._depth = self.parent._depth + 1
            self._auto_escape = self.parent._auto_escape

    def __unicode__(self):

        output = []

        escape_func = escape
        if not self._auto_escape:
            escape_func = lambda s: s

        if self.tag:
            output.append(self.beautify + "<" + self.tag)
            for a, v in self.attrs.iteritems():
                output.append(a + '="' + escape_func(str(v)) + '"')
            output = [" ".join(output) + ">"]
        if self.content:
            output.append(self.beautify + self.content)
        if self.tag in self.self_closing_tags:
            return "".join(output)
        for c in self.children:
            output.append(c.__unicode__())
        if self.tag:
            output.append(self.beautify + "</" + self.tag + ">")
        return "\n".join(output)

    @property
    def beautify(self):
        if self._beautify:
            return self._indentfunc(self._depth)
        return ""

    def __str__(self):
        return self.__unicode__().encode("utf-8")

    def __repr__(self):
        return ('<{} object tag="{}" attrs="{}" content="{}" '
                'at {}>').format(self.__class__.__name__,
                                            self.tag,
                                            repr(self.attrs),
                                            self.content,
                                            "0" + hex(id(self))[2:].upper())

    def __bool__(self):
        return bool(self.tag or self.content)

    def __nonzero__(self):
        return self.__bool__()

    def __call__(self):
        return self.__unicode__()

    def __getitem__(self, k):
        return self.attrs.get(k, None)

    def __setitem__(self, k, v):
        self.attrs[k] = v
