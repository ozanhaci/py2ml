# encoding: utf-8
import weakref
import unittest

from ..node import Node, Root, default_indentfunc


class NodeTest(unittest.TestCase):

    def test_append(self):
        root = Root()
        html = Node(parent=root, tag="html")
        body = Node(tag="body")
        html.append(body)
        self.assertIn(body, html.children)

        form = Node(tag="form")
        body.append(form)
        self.assertIn(form, body.children)

        self.assertEqual(0, root.children.index(html))
        self.assertEqual(0, html.children.index(body))
        self.assertEqual(0, body.children.index(form))

        p = Node(tag="p")
        body.append(p)

        self.assertEqual(2, len(body.children))

        self.assertEqual(0, body.children.index(form))
        self.assertEqual(1, body.children.index(p))

    def test_prepend(self):
        root = Root()
        html = Node(parent=root, tag="html")
        body = Node(tag="body")
        html.prepend(body)
        self.assertIn(html, root.children)
        self.assertIn(body, html.children)

        form = Node(tag="form")
        body.prepend(form)
        self.assertIn(form, body.children)

        self.assertEqual(0, html.children.index(body))
        self.assertEqual(0, body.children.index(form))

        p = Node(tag="p")
        body.prepend(p)

        self.assertEqual(2, len(body.children))

        self.assertEqual(0, body.children.index(p))
        self.assertEqual(1, body.children.index(form))

        head = Node(tag="head")
        html.prepend(head)

        self.assertEqual(2, len(html.children))
        self.assertEqual(0, html.children.index(head))
        self.assertEqual(1, html.children.index(body))

    def test_after(self):
        root = Root()
        form = Node(parent=root, tag="form")
        p = Node(tag="p")
        form.after(p)

        self.assertEqual(0, len(form.children))
        self.assertEqual(2, len(form.parent.children))
        self.assertEqual(0, form.parent.children.index(form))
        self.assertEqual(1, form.parent.children.index(p))

        span = Node(tag="span")
        form.after(span)
        self.assertEqual(3, len(form.parent.children))
        self.assertEqual(1, form.parent.children.index(span))

    def test_before(self):
        root = Root()
        form = Node(parent=root, tag="form")
        p = Node(tag="p")
        form.before(p)

        self.assertEqual(0, len(form.children))
        self.assertEqual(2, len(form.parent.children))
        self.assertEqual(0, form.parent.children.index(p))
        self.assertEqual(1, form.parent.children.index(form))

        span = Node(tag="span")
        form.before(span)

        self.assertEqual(3, len(form.parent.children))
        self.assertEqual(1, form.parent.children.index(span))

    def test_bool(self):
        tag1 = Node()
        tag2 = Node(tag="p")
        self.assertFalse(bool(tag1))
        self.assertTrue(bool(tag2))

    def test_beautify(self):
        root = Root(beautify=True)
        html = Node(parent=root, tag="html")
        body = Node(tag="body")
        html.append(body)

        self.assertTrue(html._beautify)
        self.assertTrue(body._beautify)
        self.assertEqual(0, html._depth)
        self.assertEqual(1, body._depth)
        self.assertEqual("", html.beautify)
        self.assertEqual(default_indentfunc(body._depth), body.beautify)

        form = Node(tag="form")
        body.append(form)

        self.assertTrue(form._beautify)
        self.assertEqual(2, form._depth)
        self.assertEqual(default_indentfunc(form._depth), form.beautify)

    def test_attrs(self):
        form_attr = {"class": "form-class"}
        form = Node(tag="form", attr=form_attr)
        self.assertEqual(form.attrs, form_attr)
        form.attr(action="/test.py")
        self.assertEqual(form.attrs["action"], "/test.py")
        form["action"] = "/test2.py"
        self.assertEqual(form.attrs["action"], "/test2.py")
        self.assertEqual(form.attrs["class"], form_attr["class"])
        self.assertEqual(2, len(form.attrs))

    def test_weakrefs(self):
        root = Root()
        html = Node(parent=root, tag="html")
        body = Node(tag="body")
        html.append(body)

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))

        form = Node(tag="form")
        body.prepend(form)

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(form.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))

        input1 = Node(tag="input")
        form.append(input1)

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(input1.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(form.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))

        input1.after(Node(content="content after input1"))

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(input1.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(form.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))

        form.before(Node(content="content before form"))

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(input1.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(form.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))

        html.after(Node(content="content after html"))

        self.assertFalse(hasattr(root, "parent"))
        self.assertTrue(isinstance(input1.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(form.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(body.parent, weakref.ProxyTypes))
        self.assertTrue(isinstance(html.parent, weakref.ProxyTypes))

    def _test_replace_child(self):
        # TODO THIS WILL BE IMPLEMENTED LATER
        pass

    def _test_disown_child(self):
        # TODO THIS WILL BE IMPLEMENTED LATER
        pass


if __name__ == "__main__":
    unittest.main(verbosity=2)
