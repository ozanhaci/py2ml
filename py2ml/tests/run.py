# encoding: utf-8
import sys
import os
import unittest
import doctest

here = os.path.dirname(__file__)
readme = os.path.normpath(os.path.join(here, '..', '..', 'README.rst'))

doctest_modules = [] # ["py2ml"]


def test_suite():
    suite = additional_tests()
    loader = unittest.TestLoader()
    for fn in os.listdir(here):
        if fn.startswith('test_') and fn.endswith(".py"):
            modname = 'py2ml.tests.' + fn[:-3]
            print("unittest mods", modname)
            __import__(modname)
            module = sys.modules[modname]
            suite.addTests(loader.loadTestsFromModule(module))
    return suite


def additional_tests():
    suite = unittest.TestSuite()
    for mod in doctest_modules:
        try:
            print("doctest mod", mod)
            suite.addTest(doctest.DocTestSuite(mod))
        except ValueError:
            pass
    if os.path.isfile(readme):
        suite.addTest(doctest.DocFileSuite(readme, module_relative=False))
    return suite


def main():
    suite = test_suite()
    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(suite)

if __name__ == '__main__':
    main()
