# encoding: utf-8
from __future__ import print_function
import unittest

from ..node import Node, Root, default_indentfunc


class OutputTest(unittest.TestCase):

    def test_output_type(self):
        root = Root()
        html = Node(root, tag=b"html")
        body = html.append(b"body")
        body["style"] = b"width:100%"
        body.append(Node(content=b"Body Content"))
        self.assertIsInstance(str(root), str)
        self.assertIsInstance(unicode(root), unicode)
        self.assertIsInstance(str(body), str)
        self.assertIsInstance(unicode(body), unicode)

        uroot = Root()
        uhtml = Node(root, tag=u"html")
        ubody = uhtml.append(u"body")
        ubody[u"style"] = u"width:100%"
        ubody.append(Node(content=u"Body Content"))
        ubody.append(Node(content=u"çığöşüÇIĞÖŞÜäßÄ"))
        self.assertIsInstance(unicode(uroot), unicode)
        self.assertIsInstance(str(uroot), str)
        self.assertIsInstance(unicode(ubody), unicode)
        self.assertIsInstance(str(ubody), str)

    def test_output(self):
        root = Root()
        html = Node(root, tag="html")
        form = html.append("form")
        output = str(root)
        self.assertEqual('<html>\n'
                         '<form>\n'
                         '</form>\n'
                         '</html>', output)

        form["id"] = "id_form"
        output = str(root)
        self.assertEqual('<html>\n'
                         '<form id="id_form">\n'
                         '</form>\n'
                         '</html>', output)
        form["data-test"] = "test'test"
        output = str(root)
        self.assertEqual('<html>\n'
                         '<form id="id_form" data-test="test&#39;test">\n'
                         '</form>\n'
                         '</html>', output)
        form.after(Node(content="content1"))
        output = str(root)
        self.assertEqual('<html>\n'
                         '<form id="id_form" data-test="test&#39;test">\n'
                         '</form>\n'
                         'content1\n'
                         '</html>', output)

        form.before(Node(content="content2"))
        output = str(root)
        self.assertEqual('<html>\n'
                         'content2\n'
                         '<form id="id_form" data-test="test&#39;test">\n'
                         '</form>\n'
                         'content1\n'
                         '</html>', output)

        inpt1 = form.append("input")
        inpt1["type"] = "password"
        inpt1["name"] = "password"
        output = str(root)
        self.assertEqual('<html>\n'
                         'content2\n'
                         '<form id="id_form" data-test="test&#39;test">\n'
                         '<input type="password" name="password">\n'
                         '</form>\n'
                         'content1\n'
                         '</html>', output)

    def test_beautified_output(self, indfunc=None):
        indfunc = indfunc or default_indentfunc
        root = Root(beautify=True, indentfunc=indfunc)
        html = Node(root, tag="html")
        form = html.append("form")
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(form._depth) + '<form>\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(html._depth) + '</html>', output)

        form["id"] = "id_form"
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(form._depth) + '<form id="id_form">\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(html._depth) + '</html>', output)
        form["data-test"] = "test'test"
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(form._depth) +
                         '<form id="id_form" data-test="test&#39;test">\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(html._depth) + '</html>', output)
        content1 = form.after(Node(content="content1"))
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(form._depth) +
                         '<form id="id_form" data-test="test&#39;test">\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(content1._depth) + 'content1\n' +
                         indfunc(html._depth) + '</html>', output)

        content2 = form.before(Node(content="content2"))
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(content2._depth) + "content2\n" +
                         indfunc(form._depth) +
                         '<form id="id_form" data-test="test&#39;test">\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(content1._depth) + 'content1\n' +
                         indfunc(html._depth) + '</html>', output)

        inpt1 = form.append("input")
        inpt1["type"] = "password"
        inpt1["name"] = "password"
        output = str(root)
        self.assertEqual(indfunc(html._depth) + '<html>\n' +
                         indfunc(content2._depth) + 'content2\n' +
                         indfunc(form._depth) +
                         '<form id="id_form" data-test="test&#39;test">\n' +
                         indfunc(inpt1._depth) +
                         '<input type="password" name="password">\n' +
                         indfunc(form._depth) + '</form>\n' +
                         indfunc(content1._depth) + 'content1\n' +
                         indfunc(html._depth) + '</html>', output)

    def test_beautified_output_tab(self):
        indfunc = lambda depth: "\t"*depth
        return self.test_beautified_output(indfunc)


if __name__ == "__main__":
    unittest.main(verbosity=2)
